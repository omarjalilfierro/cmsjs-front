module.exports = {
  mainNav: {
    signUp: 'Crear Cuenta',
    signIn: 'Iniciar Sesión',
    logout: 'Cerrar Sesión',
  },
  signupPage: {
    title: 'Crear Cuenta',
    basicSignUpAction: 'Crea Una Cuenta Hoy',
  },
  signinPage: {
    title: 'Bienvenido de vuelta',
    basicSignInAction: 'Iniciar Sesión',
  },
};
