module.exports = {
  mainNav: {
    signUp: "S'enregistrer",
    signIn: 'Se Connecter',
    logout: 'Se Deconnecter',
  },
  signupPage: {
    title: "S'enregistrer",
    basicSignUpAction: "S'enregistrer maintenant",
  },
  signinPage: {
    title: 'Bon retour',
    basicSignInAction: 'Se Connecter',
  },
};
